import java.util.Scanner;

public class Main {

    public static int findMin(int size, int a[]) {
        int min=a[0];
        for (int i=0; i<size; i++) {
            if (a[i]<=min) {
                min=a[i];
            }
        }
        return min;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();

        int a[]= new int[size];
        for(int i=0; i<size; i++) {
            a[i] = s.nextInt();
        }

        System.out.println(findMin(size, a));
    }
}
